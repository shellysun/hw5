<!doctype html>
<?php
require('mlib_functions.php');
html_head("Mlib - Media Library");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add Media</h2>
  <form action="mlib_media.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>Title</td>
        <td align="left"><input type="text" name="Title" size="35" maxlength="35"></td>
     
      </tr>
		
		<tr>
        <td>Author</td>
        <td align="left"><input type="text" name="Author" size="35" maxlength="35"></td>
      
      </tr>



<tr>        
<td>Description</td>
        <td align="left"><input type="text" name="description" size="60" maxlength="70"></td>
      </tr>
      
<tr>
        <td>Type</td>
        <td align="left"><input type="text" name="Type" size="35" maxlength="35"></td>
      
      </tr>
	  <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {
  $Title = $_POST['Title'];
  $Author = $_POST['Author'];
  $description = $_POST['description'];
  $Type = $_POST['Type'];

  print "<h2>Media Added</h2>";
  print "<table border=1>";
  print "<tr>";
  print "<td>Title</td><td>Author</td><td>Description</td><td>Type</td>";
  print "</tr>";
  print "<tr>";
  print "<td>".$Title."</td>";
  print "<td>".$Author."</td>";
  print "<td>".$description."</td>";
  print "<td>".$Type."</td>";
  print "</tr>";
  print "</table>";
}
require('mlib_footer.php');
?>
